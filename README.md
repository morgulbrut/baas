# baas

bleep as a service

Exploring ways to bring bleep to the people in this dark ages of COVID-19

Will probably be in the wild during rC3

## Idea so far

* A RasPi livestreaming modular synth.
    * The RasPi is connected over a MIDI interface with the synth.
    * (Optional: The RasPi is connected to a DMX interface for some lightning).
    * (Optional: The RasPi is connected over OPC to a LED matrix).
* Any user can send requests to change some of the CV values.
    * Requests can either be sent over LoRaWAN, Telegrambot, Twitterbot.
    * During some times it will also be possible to call and ask a human operator to turn some knobs.
    * (Optional: Use Bluetooth from nearby to change some values).
* The RasPi processes the requests using n8n or node-red and sends the new values over the connected digital interfaces.

![](imgs/Idea.jpg)

## Material so far

* 1x NerdSeq with IO Expander
    * should give 6 MIDI channels with CV, Gate and Mod each.
    * Has a SEGA controller port (this calls for cool hacks)
* Bunch of modules
* RasPi
* USB MIDI interface
* Camera (RasPi or Webcam)

## What's done so far

* Got to talk to my NerdSeq (isn't that stable when you just throw random stuff at it)
* Wrote some python code to send MIDI messages
* Found out about sendMIDI and recieveMIDI, two very UNIX MIDI commandline tools

## TODO

* Install the MIDI stuff on a RasPi
* Connect and configure audio and video on the RasPi
* Install and program the workflow stuff
* Make a wilder patch
* (Optional: Getting the Sega Controller stuff done)

## Resources

* Read Sega controller from arduino: https://jonthysell.com/2014/07/26/reading-sega-genesis-controllers-with-arduino/
* Sega controller arduino library: https://github.com/jonthysell/SegaController/tree/master/src
