#!/usr/bin/python3

import mido
import time
import sys

port = mido.open_output(mido.get_output_names()[0])

def play_note(ch,n,v,t):
    note_off(ch)
    time.sleep(0.01)
    note_on(n,v,ch)
    time.sleep(t)
    note_off(ch)

def note_on(n,v=100,ch=1):
    ch -=1
    msg = mido.Message('note_on', note=n,velocity=v,channel=ch,time=20)
    port.send(msg)

def note_off(ch=1):
    ch -=1
    msg = mido.Message('note_off',velocity=64,channel=ch)
    port.send(msg)


if __name__ == "__main__":
    ch = int(sys.argv[1])
    n = int(sys.argv[2])
    v = int(sys.argv[3])
    t = int(sys.argv[4])

    play_note(ch,n,v,t)