import mido
import time
import sys

port = mido.open_output(mido.get_output_names()[0])

def send_mod(ch,v):
    ch -=1
    msg = mido.Message('control_change', control=1,value=v,channel=ch)
    port.send(msg)


if __name__ == "__main__":
    ch = int(sys.argv[1])
    v = int(sys.argv[2])

    send_mod(ch,v)