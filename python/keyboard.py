#!/usr/bin/python3

import mido

with mido.open_input(mido.get_input_names()[0]) as inport:
    for msg in inport:
        if msg.type != 'clock':
            print(msg.dict())
