#!/usr/bin/python3

import mido
import time

port = mido.open_output(mido.get_output_names()[0])

def play_note(n,t,v=64):
    port.send(mido.Message('note_on', note=n,velocity=v))
    time.sleep(t)
    port.send(mido.Message('note_off', note=n))


play_note(60,0.5,1)
play_note(63,0.4,50)
play_note(64,0.5,100)

mid = mido.MidiFile('feliz_navidad.mid')

print(mid.type)

for msg in mid.play():
    port.send(msg)