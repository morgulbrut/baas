#!/usr/bin/python3
import mido
import time

port = mido.open_output(mido.get_output_names()[0])

def play_note(n,t,v=100,ch=1):
    note_on(n,v,ch)
    time.sleep(t/2)
    note_off(ch)
    time.sleep(t/2)

def note_on(n,v=100,ch=1):
    ch -=1
    msg = mido.Message('note_on', note=n,velocity=v,channel=ch,time=20)
    port.send(msg)

def note_off(ch=1):
    ch -=1
    msg = mido.Message('note_off',velocity=64,channel=ch)
    port.send(msg)

def send_mod(v,ch=1):
    ch -=1
    msg = mido.Message('control_change', control=1,value=v,channel=ch)
    port.send(msg)

def send_pitchwheel(v,ch=1):
    ch -=1
    msg = mido.Message('pitchwheel',pitch=v,channel=ch)
    port.send(msg)


if __name__ == "__main__":   
    
    for c in range(1,17):
        print("Testing channel {}".format(c))
        send_mod(1,ch=c)
        play_note(40,0.5,ch=c)
        send_mod(50,ch=c)
        play_note(45,0.5,ch=c)
        send_mod(120,ch=c)
        play_note(42,0.5,ch=c)
